<?php

namespace App\Controller;
use Doctrine\DBAL\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MessageController extends AbstractController
{
    /**
     * @Route("/message", name="messages_create", methods={"POST"})
     */
    public function create(Request $request, HttpClientInterface $client): Response{
        try
        {
            $response = $client->request('POST','http://administration_api:80/messages', [
                'json'=>json_decode($request->getContent()),
                'auth_bearer'=> $request->headers->get("authorization")
            ]);
            return new Response($response->getContent(), $response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),$response->getStatusCode());
        }
    }


    /**
     * @Route("/message", name="messages_get_all", methods={"GET"})
     */
    public function getAll(Request $request, HttpClientInterface $client): Response{
        try
        {
            $response = $client->request('GET','http://administration_api:80/messages', [
                'auth_bearer'=> $request->headers->get("authorization")
            ]);
            return new Response($response->getContent(), $response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),$response->getStatusCode());
        }
    }

    /**
     * @Route("/message/{id}", name="message_get", methods={"GET"})
     */
    public function getOne(Request $request, HttpClientInterface $client): Response {
        try
        {
            $response = $client->request('GET','http://administration_api:80/messages/'.$request->attributes->get("id"), [
                'auth_bearer'=> $request->headers->get("authorization")
            ]);
            return new Response($response->getContent(), $response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),$response->getStatusCode());
        }
    }

    /**
     * @Route("/message/{id}", name="message_update", methods={"PUT"})
     */
    public function update(Request $request, HttpClientInterface $client): Response {
        try
        {
            $response = $client->request('PUT',
                'http://administration_api:80/messages/'.$request->attributes->get("id"), [
                    'json'=>json_decode($request->getContent()),
                    'auth_bearer'=> $request->headers->get("authorization")
                ]
            );
            return new Response($response->getContent(), $response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),$response->getStatusCode());
        }
    }

    /**
     * @Route("/message/{id}", name="message_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HttpClientInterface $client): Response {
        try
        {
            $response = $client->request('DELETE',
                'http://administration_api:80/messages/'.$request->attributes->get("id"), [
                    'auth_bearer'=> $request->headers->get("authorization")
                ]
            );
            return new Response($response->getContent(), $response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),$response->getStatusCode());
        }
    }
}
