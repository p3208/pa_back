<?php

namespace App\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/missions")
 */
class MissionController extends AbstractController
{
    /**
     * @Route("", name="getAll_mission", methods={"GET"})
     */
    public function getAll(Request $request, HttpClientInterface $client): Response
    {
        try
        {
            $response = $client->request('GET','http://mission_api:80/missions',['auth_bearer'=> $request->headers->get("Authorization")
            ]);
            return new Response($response->getContent(),$response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),$response->getStatusCode());
        }
    }
    /**
     * @Route("", name="create_mission", methods={"POST"})
     */
    public function create(Request $request, HttpClientInterface $client): Response
    {
        try
        {
            $response = $client->request('POST','http://mission_api:80/missions',['json'=>json_decode($request->getContent()),
                'auth_bearer'=> $request->headers->get("Authorization")
            ]);
            return new Response($response->getContent(),$response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),$response->getStatusCode());
        }
    }

    /**
     * @Route("/{id}", name="update_mission", methods={"PUT"})
     */
    public function update(Request $request, HttpClientInterface $client, int $id): Response
    {
        try
        {
            $response = $client->request('PUT','http://mission_api:80/missions/'.$id,['json'=>json_decode($request->getContent()),
                'auth_bearer'=> $request->headers->get("Authorization")
            ]);
            return new Response($response->getContent(),$response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),$response->getStatusCode());
        }
    }

    /**
     * @Route("/{id}", name="delete_mission", methods={"DELETE"})
     */
    public function delete(Request $request, HttpClientInterface $client, int $id): Response
    {
        try
        {
            $response = $client->request('DELETE','http://mission_api:80/missions/'.$id,['auth_bearer'=> $request->headers->get("authorization")
            ]);
            return new Response($response->getContent(),$response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),$response->getStatusCode());
        }
    }

    /**
     * @Route("/{id}", name="getOne_mission", methods={"GET"})
     */
    public function getOne(Request $request, HttpClientInterface $client, int $id): Response
    {
        try
        {
            $response = $client->request('GET','http://mission_api:80/missions/'.$id,['auth_bearer'=> $request->headers->get("Authorization")
            ]);
            return new Response($response->getContent(),$response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),$response->getStatusCode());
        }
    }
}
