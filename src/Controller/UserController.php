<?php

namespace App\Controller;

use Doctrine\DBAL\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="users", methods={"GET"})
     */
    public function getAll(Request $request, HttpClientInterface $client): Response{
        try
        {
            $response = $client->request('GET','http://user_api:80/users', [
                'auth_bearer'=> $request->headers->get("authorization")
            ]);
            return new Response($response->getContent(), $response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e), 500);
        }
    }

    /**
     * @Route("/user/{id}", name="user_get", methods={"GET"})
     */
    public function getOne(Request $request, HttpClientInterface $client): Response {
        try
        {
            $response = $client->request('GET','http://user_api:80/users/'.$request->attributes->get("id"), [
                'auth_bearer'=> $request->headers->get("authorization")
            ]);
            return new Response($response->getContent(), $response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e), 500);
        }
    }

    /**
     * @Route("/user/{id}", name="user_update", methods={"PUT"})
     */
    public function update(Request $request, HttpClientInterface $client): Response {
        try
        {
            $response = $client->request('PUT',
                'http://user_api:80/users/'.$request->attributes->get("id"), [
                    'json'=>json_decode($request->getContent()),
                    'auth_bearer'=> $request->headers->get("authorization")
                ]
            );
            return new Response($response->getContent(), $response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e), 500);
        }
    }

    /**
     * @Route("/user/{id}", name="user__delete", methods={"DELETE"})
     */
    public function delete(Request $request, HttpClientInterface $client): Response {
        try
        {
            $response = $client->request('DELETE',
                'http://user_api:80/users/'.$request->attributes->get("id"), [
                    'auth_bearer'=> $request->headers->get("authorization")
                ]
            );
            return new Response($response->getContent(), $response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e), 500);
        }
    }
}
