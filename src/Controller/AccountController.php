<?php

namespace App\Controller;
use Doctrine\DBAL\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AccountController extends AbstractController
{
    /**
     * @Route("/connexion", name="login_account", methods={"POST"})
     */
    public function login(Request $request, HttpClientInterface $client): Response
    {
        try
        {
            $response = $client->request('POST','http://user_api:80/authentication_token',['json'=>json_decode($request->getContent())
            ]);
            return new Response($response->getContent(),200);
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),500);
        }
    }

    /**
     * @Route("/inscription", name="register_account", methods={"POST"})
     */
    public function register(Request $request, HttpClientInterface $client): Response
    {
        try
        {
            $response = $client->request('POST','http://user_api:80/users',['json'=>json_decode($request->getContent())
            ]);
            return new Response($response->getContent(),$response->getStatusCode());
        }
        catch (Exception $e)
        {
            return new Response(json_encode($e),$response->getStatusCode());
        }
    }
}
